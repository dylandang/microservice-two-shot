from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder
    }

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats,
            HatListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0},
        )
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_href = content["location"]
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats,
            HatDetailEncoder,
            safe=False,
        )
