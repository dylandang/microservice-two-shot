# Wardrobify

Team:

* Person 1 - Which microservice?
Dylan Dang - Hats
* Person 2 - Which microservice?
Christian Hanna - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

I create a view with a GET and POST method witch allow me to create a "shoe" using the model and display the list of them.
For the part of the shoe that is the bin I use a shoe poll for polling that data since it is not in the microservice of the shoe, but in the wardrobe microservice.
I also have a view with GET, PUT, and DELETE. In React I worked to display the list of shoes, as well as a place to input data to create a hat that would then be stored. I worked on a function that would allow an individual hat to be delete.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I created a GET and POST method for hats to display a list of hats and create a hat. Also, I created a GET, PUT and DELETE method for hats to display ID of hats, update ID of hats and delete ID of hats. I added a path in the urls.py for the functions to link to. For react, I created a list function to display a list of hats when on a certain URL. Also, I created a form function to create hats with a form to fill out. Lastly, I added a function to delete hats on the website.
