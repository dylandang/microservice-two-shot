import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import React, { useEffect, useState } from 'react';
import HatList from './HatList';
import HatForm from './HatForm';
import React, { useEffect, useState } from 'react';

function App() {
  const [shoes, setShoe] = useState([])
  const [bins, setBin] = useState([])

  const fetchShoe = async () => {
    const url = "http://localhost:8080/api/shoes/";
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setShoe(data.shoes)
    }
  }

const fetchBin = async () => {
  const url = "http://localhost:8100/api/bins/";
  const response = await fetch(url)
  if (response.ok) {
    const data = await response.json()
    setBin(data.bins)
  }
}
useEffect(() => {
  fetchShoe();
  fetchBin();
}, [setShoe, setBin]);

if (bins === undefined) {
    return null;
  } else if (shoes === undefined) {
    return null;
  }

  const [hats, setHat] = useState([])
  const [locations, setLocation] = useState([])

  const fetchHat = async () => {
  const url = "http://localhost:8090/api/hats/";
  const response = await fetch(url)
  if (response.ok) {
    const data = await response.json()
    setHat(data.hats)
  }
}


  const fetchLocation = async () => {
  const url = "http://localhost:8100/api/locations/";
  const response = await fetch(url)
  if (response.ok) {
      const data = await response.json()
      setLocation(data.locations)
      }
  }
  useEffect(() => {
    fetchHat();
    fetchLocation();
}, [setHat, setLocation]);

if (locations === undefined) {
    return null;
  } else if (hats === undefined) {
    return null;
}
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoeList shoes={shoes} />} />
          <Route path="shoes/new" element={<ShoeForm bins={bins} />} />
          <Route path="hats" element={<HatList hats={hats} />} />
          <Route path="hats/new" element={<HatForm locations={locations} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
