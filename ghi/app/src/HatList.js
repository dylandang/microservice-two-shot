import React from 'react'

async function deleteHat(hat) {
    const hatUrl = `http://localhost:8090/api/hats/${hat.id}`;
    const fetchConfig = {
        method: "delete",
        headers: {
            'Content-Type': 'application/json',
        },
    };
    await fetch(hatUrl, fetchConfig)
    window.location.reload(true)
}

function HatList(props) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style Name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Location</th>
            </tr>
          </thead>
          <tbody>
            {props.hats.map(hat => {
              return (
                <tr key={hat.id}>
                  <td>{ hat.fabric }</td>
                  <td>{ hat.style_name }</td>
                  <td>{ hat.color }</td>
                  <td>
                    <img src={ hat.picture_url } alt="" width="40" height="40" />
                  </td>
                  <td>{ hat.location }</td>
                  <td>
                    <button className="btn btn-danger" onClick={() => deleteHat(hat)}>Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
}
export default HatList