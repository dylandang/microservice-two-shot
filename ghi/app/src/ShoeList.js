import React from 'react'

async function deleteShoe(shoe) {
    const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
    const fetchConfig = {
        method: "delete",
        headers: {
            'Content-Type': 'application/json',
        },
    };
    await fetch(shoeUrl, fetchConfig)
    window.bin.reload(true)
}

function ShoeList(props) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model Name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Bin</th>
            </tr>
          </thead>
          <tbody>
            {props.shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.color }</td>
                  <td>
                    <img src={ shoe.picture_url } alt="" width="40" height="40" />
                  </td>
                  <td>{ shoe.bin }</td>
                  <td>
                  <button className="btn btn-danger" onClick={() => deleteShoe(shoe)}>Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
}
export default ShoeList
