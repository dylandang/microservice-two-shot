import React, { useState } from 'react';

function ShoeForm(props) {
    const [manufacturer, setManufacturer] = useState('')
    const [modelName, setModelName] = useState('')
    const [color, setColor] = useState('')
    const [picture, setPicture] = useState('')
    const [bin, setBin] = useState('')

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }
    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    const handlePictureChange = (event) => {
        const value = event.target.value
        setPicture(value)
    }
    const handleBinChange = (event) => {
        const value = event.target.value
        setBin(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = picture;
        data.bin = bin;
        console.log(data)
        const shoeUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const shoeResponse = await fetch(shoeUrl, fetchConfig);
        if (shoeResponse.ok) {
            const newShoe = await shoeResponse.json();
            console.log(newShoe)

        setManufacturer('')
        setModelName('')
        setColor('')
        setPicture('')
        setBin('')
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-bin-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                    <label htmlFor="model_name">ModelName</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} value={picture} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a Bin</option>
                        {props.bins.map(bin => {
                            return (
                            <option key={bin.id} value={bin.href}>
                                {bin.closet_name}
                            </option>
                            );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;
